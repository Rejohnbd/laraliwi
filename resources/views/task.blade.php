<x-guest-layout>
    @if (Route::has('login'))
    <livewire:welcome.navigation />
    @endif
    <div>
        <livewire:task.task-component />
    </div>
</x-guest-layout>