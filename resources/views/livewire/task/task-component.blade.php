<div>
    <h1>Task</h1>

    {{-- <input type="text" wire:model="task" placeholder="Task">
    <button wire:click="add" class="bg-indigo-500 p-2 rounded">Add Task</button> --}}

    {{-- <form wire:submit="add">
        <input type="text" wire:model="task" placeholder="Task">
        <button wire:click="add" class="bg-indigo-500 p-2 rounded">Add Task</button>
    </form> --}}

    <input type="text" wire:model="task" placeholder="Task">
    <button wire:mouseenter="add" class="bg-indigo-500 p-2 rounded">Add Task</button>
    <ul>
        @foreach($tasks as $task)
        <li>{{ $task }}</li>
        @endforeach
    </ul>
</div>