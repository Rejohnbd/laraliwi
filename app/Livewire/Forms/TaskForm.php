<?php

namespace App\Livewire\Forms;

use Livewire\Attributes\Rule;
use Livewire\Attributes\Validate;
use Livewire\Form;

class TaskForm extends Form
{
    #[Rule('required', message: 'Title is required')]
    #[Rule('min:3', message: 'Title Minium length is 3')]
    public $title;
    #[Rule('required')]
    #[Rule('min:3', message: 'Title Minium length is 3')]
    public $slug;
    #[Rule('required|min:3')]
    public $description;
    #[Rule('required')]
    public $status;
    #[Rule('required')]
    public $priority;
    #[Rule('required')]
    public $deadline;

    public function createTask()
    {
        auth()->user()->tasks()->create($this->all());
        request()->session()->flash('success', 'Task Created');
    }
}
