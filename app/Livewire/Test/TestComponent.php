<?php

namespace App\Livewire\Test;

use Livewire\Component;

class TestComponent extends Component
{
    // public $name = 'Hi Rejohn';

    public function render()
    {
        return view('livewire.test.test-component');
    }
}
