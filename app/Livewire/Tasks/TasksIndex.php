<?php

namespace App\Livewire\Tasks;

use App\Enums\PriorityType;
use App\Enums\StatusType;
use App\Livewire\Forms\TaskForm;
use Livewire\Component;

class TasksIndex extends Component
{
    public TaskForm $form;

    public function render()
    {
        $statusTypes = StatusType::cases();
        $prriorityTypes = PriorityType::cases();
        // dd($statusTypes, $prriorityTypes);
        return view('livewire.tasks.tasks-index', compact('statusTypes', 'prriorityTypes'))->layout('layouts.app');
    }


    public function save()
    {
        $this->validate();
        $this->form->createTask();
        $this->form->reset();
    }
}
